defmodule Issues.GithubIssues do
  require Logger

  @github_url Application.get_env(:issues, :github_url)

  @spec fetch(String.t(), String.t()) :: {:ok, any} | {:error, any}
  def fetch(user, project) do
    Logger.info("Fetching #{user}'s project #{project}")
    issues_url = "#{@github_url}/repos/#{user}/#{project}/issues"

    issues_url
    |> HTTPoison.get()
    |> handle_response()
  end

  @spec handle_response({any, HTTPoison.Response.t()}) :: {:ok, any} | {:error, any}
  def handle_response({:ok, %{status_code: 200, body: body}}) do
    {:ok, Poison.decode!(body)}
  end

  def handle_response({_, %{status_code: status_code, body: body}}) do
    {
      status_code |> check_for_error(),
      body |> Poison.decode!()
    }
  end

  defp check_for_error(200), do: :ok
  defp check_for_error(_), do: :error
end
